/**
 * 选中dom元素
 */
export function selector(elem) {
    return elem.nodeType ? elem : document.querySelector(elem);
}

/**
 * 警告提示
 */
export function warn() {
    let arr = [];
    for (let i = 0; i < arguments.length; i++) {
        arr.push(`${i + 1}. ${arguments[i]}`);
    }
    console.warn(arr.join('\n'));
}

/**
 * 安全拷贝数据
 */
export function safety(data) {
    return JSON.parse(JSON.stringify(data));
}

/**
 * 检测对象是否为数组
 */
export function isArray(obj) {
    return Object.prototype.toString.call(obj) === "[object Array]";
}

/**
 * 检测对象是否为函数
 */
export function isFunction(obj) {
    return Object.prototype.toString.call(obj) === "[object Function]";
}

/**
 * 简单的深度合并
 */
export function deepMerge(obj1, obj2) {
    let key;
    for (key in obj2) {
        obj1[key] =
            obj1[key] &&
            obj1[key].toString() === "[object Object]" &&
            (obj2[key] && obj2[key].toString() === "[object Object]") ?
                deepMerge(obj1[key], obj2[key]) :
                (obj1[key] = obj2[key]);
    }
    return obj1;
}
