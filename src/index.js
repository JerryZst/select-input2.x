import packageJson from '../package.json'
import {selector} from '@/utils/index'
import SelectInput from 'components/select-input/index'

const {name, version, author} = packageJson;

export const dataS = {};
export const optionData = {};
export const componentData = {};

export default {
    name,
    version,
    author,
    render(options) {
        let {elem} = options;
        options.dom = selector(elem);
        if (elem.nodeType) {
            let id = "SELECT_INPUT_RENDER_" + Date.now() + '_' + Math.random();
            elem.setAttribute(name, id);
            elem = `[${name}='${id}']`
            options.elem = elem;
        }
        optionData[elem] = options;
        let instance = new SelectInput(options);
        if (instance && instance.options.render_success) {
            dataS[elem] = instance;
        }
        return instance;
    },
    get(filter, single) {
        let type = Object.prototype.toString.call(filter);
        let method;
        switch (type) {
            case '[object String]':
                filter && (method = item => item === filter);
                break;
            case '[object RegExp]':
                method = item => filter.test(item);
                break;
            case '[object Function]':
                method = filter;
                break;
            default:
                break;
        }
        let keys = Object.keys(dataS)
        let list = (method ? keys.filter(method) : keys).map(key => dataS[key]).filter(instance => selector(instance.options.elem));

        return single ? list[0] : list;
    },
    batch(filter, method) {
        let args = [...arguments];
        args.splice(0, 2);
        return this.get(filter).map(instance => instance[method](...args));
    },
}
