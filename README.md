## 这是个啥？

selectInput2.x 是selectInput的升级版本 是将input框变成即可输入亦可选择的select下拉组件，主要使用场景是用户可以输入关键词，匹配系统存在的联想词形成select下拉框以供用户选择，当然用户也可以使用自行输入的信息，支持模糊匹配，数据源可以本地赋值，也可以异步url请求加载，或者干脆点直接实时输入远程请求联想呈现

selectInput2.x 借鉴  [xm-select](https://gitee.com/maplemei/xm-select) 的开发方式, 利用preact进行渲染, 大幅度提高渲染速度, 并且可以灵活拓展，感谢xm-select的作者

*****

## 软件架构
1. 引入第三方[preact](https://preactjs.com/)库, 利用jsx渲染页面结构
2. 使用[webpack](https://www.webpackjs.com/)进行打包


## 怎么使用呢?

1. 下载源代码, 通常情况下, 你可以在 [Releases](https://gitee.com/JerryZst/select-input2.x/releases) 这里找到所有已经发布的版本.
2. 将下载好的文件, 通常是压缩包, 解压到你项目的扩展目录里去, 譬如: `libs/modules`
3. 直接引入打包好的js文件即可，通常是dist目录的select-input.js
4. 具体使用可参考 [示例文件](./dist/index.html
5. 示例代码仅供参考，请勿生搬照抄，代码出现报错，概不负责，谢谢

## 快速使用

```
1. 引入 `dist/select-input.js`
2. 写一个`<div id="demo"></div>`
3. 渲染
	let ins = selectInput.render({
		elem: '#demo', 
		data: [
			{name: '小米', value: 1},
			{name: '苹果', value: 2},
			{name: '华为', value: 3},
			{name: 'vivo', value: 4}
		]
   })
```

## 文档

[组件参数文档](./select-input2.x.md)

## 支持作者

最简单的粗暴的方式就是直接使用你们的**钞能力**, 当然这是您自愿的! **点击可直接查看大图**

### 赞赏通道

<img src="./images/zhifubao.jpg" height="200px" title="支付宝">
<img src="./images/wxpay.jpg" height="200px" title="微信">
<img src="./images/qqpay.jpg" height="200px" title="QQ">

当然, 如果客观条件不允许或者主观上不愿意, 也没啥关系嘛! 码农的钱挣得都是辛苦钱啊. 所以除了使用 **钞能力**, 你还可以通过以下方式支持作者!

1. 给项目点个 Star, 让更多的小伙伴知道这个扩展!
2. 积极测试, 反馈 BUG, 如果发现代码中有不合理的地方积极反馈!
3. 加入粉丝群, 看看有多少志同道合的小伙伴! <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=5qRRxB1XRBphoKwyNGe7vZ_b_WPlgQPb&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="selectInput交流" title="selectInput交流"></a>

<img title="QQ群" src='https://images.gitee.com/uploads/images/2021/0617/162328_bedc2fe6_1476573.png' width="200px" height="200px"></img>
<img title="微信群" src='./images/wechat.png' width="200px" height="200px"></img>

