const pkg = require('./package.json');
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require("terser-webpack-plugin");
const {
    CleanWebpackPlugin
} = require('clean-webpack-plugin');

const isProd = process.env.NODE_ENV === 'prod';

const banner =
    `@Title: ${pkg.name}
@Version: ${pkg.version}
@Description：Layui select配合input实现可输入，可选择，可搜索，支持异步加载，远程搜索，也可以本地data直接赋值，主要使用场景是select框可以自己输入，就是在下拉列表里找不到自己想要的选项就可以自己输入，同时还要支持模糊匹配功能，数据源可以从本地赋值，也可以异步url请求加载，或者直接远程请求联想
@Site: https://gitee.com/JerryZst/select-input2.x
@Author: flechazo
@License：MIT`;

const webpackConfig = {
    entry: {
        'selectInput': "./src/main.js"
    },
    output: {
        path: path.resolve(__dirname, isProd ? './dist/' : './build/'),
        filename: '[name].js'
    },
    mode: 'development', // 设置mode
    module: {
        rules: [{
            test: /\.css$/,
            use: ['style-loader', 'css-loader'],
        }, {
            test: /\.less$/,
            exclude: /node_modules/,
            use: ['style-loader', 'css-loader', 'less-loader'],
        }, {
            test: /\.m?js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env']
                }
            }
        }]
    },
    resolve: {
        alias: {
            '@': path.resolve(__dirname, "./src"),
            'components': path.resolve(__dirname, "./src/components"),
            'style': path.resolve(__dirname, "./src/style")
        }
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './docs/index.ejs',
            filename: 'index.html',
            // favicon: './examples/favicon.ico',
            scriptLoading: 'blocking',
            minify: {
                collapseWhitespace: true
            }
        }),
        new webpack.BannerPlugin(banner),
    ],
    optimization: {
        minimize: true,//可以自行配置是否压缩
        minimizer: [
            new TerserPlugin({
                extractComments: false,//不将注释提取到单独的文件中
            }),
        ],
    },
    devServer: {
        host: '0.0.0.0',
        port: 8888,
        hot: true,
        setupMiddlewares: (middlewares, devServer) => {
            // 设置 mock API 路由
            devServer.app.get('/api/menu_options', (req, res) => {
                const data = [
                    { name: 'BBB', value: 1 },
                    { name: 'AAA', value: 2 },
                    { name: 'CCC', value: 3 },
                ];

                res.json({
                    code: 200,
                    data,
                });
            });

            return middlewares;
        },
    },
};

if (isProd) {
    webpackConfig.plugins.push(
        new CleanWebpackPlugin(),
    )
}


module.exports = webpackConfig;
