#### 介绍

select-input2.x 是selectInput的升级版本，selectInput2.x 借鉴 [xm-select](https://gitee.com/maplemei/xm-select) 的开发方式, 利用preact进行渲染, 大幅度提高渲染速度, 并且可以灵活拓展

#### 新版本组件实例参数，大部分沿用老版本的参数，新版本不再支持layui的输入事件和选择事件监听，统一修改为实例参数传入回调函数监听处理


| 字段          | 类型     | 描述                                                         | 是否必须                                                |
| ------------- | -------- | ------------------------------------------------------------ | ------------------------------------------------------- |
| elem          | string   | 容器id                                                       | 是                                                      |
| name          | string   | 渲染的input的name值                                          | 否                                                      |
| height          | string   | 渲染的下拉框的最大高度，超过自动出现滚动条                                    | 否(默认: 200px)                                                      |
| layFilter     | string   | 同layui form参数lay-filter                                   | 否                                                      |
| layVerify     | string   | 同layui form参数lay-verify                                   | 否                                                      |
| layVerType    | string   | 同layui form参数lay-verType                                  | 否                                                      |
| layReqText    | string   | 同layui form参数lay-ReqText                                  | 否                                                      |
| hasSelectIcon | boolean  | 是否显示select下拉显示图标                                   | 否(默认false)                                           |
| method        | string   | 一次性请求服务端加载data的方法                               | 否(默认为get)                                           |
| params        | object   | 一次性请求服务端加载data的所需的参数                         | 否(默认为{})                                            |
| statusCode    | string   | 服务端响应状态码的key                                        | 否(默认code)                                            |
| statusOK      | int      | 服务端响应状态码的成功返回的值                               | 否(默认0)                                               |
| statusData    | string   | 服务端响应值的key                                            | 否(默认data)                                            |
| statusMessage | string   | 服务端响应消息的key                                          | 否(默认msg)                                             |
| invisibleMode | boolean  | 实例方法getValue返回数据模式<br>(调用getValue方法获取值，此处可能有用户自行输入的值，<br>此时拿不到传入的对象里面的value值，只能返回实时input的value值，<br>故新增实例参数invisibleMode，默认为false，<br>此项参数如果为true在调用实例方法getValue方法时返回的是对象，<br>如：{"value":"21111","isSelect":false}，<br>对象属性value即为实例value值，而isSelect则表示是否是选中的值，而非用户自行输入的值) | 否 (默认false)                                          |
| hasInitShow   | boolean  | 组件渲染完是否立即展开下拉选项                               | 否 (默认false)                                          |
| hasCut        | boolean  | 是否启用input剪贴事件                                        | 否 (默认true，false即为禁用)                            |
| ignoreCase    | boolean  | 前端联想匹配是否忽略大小写                                   | 否 (默认false，false即为强匹配，true即为忽略大小写匹配) |
| placeholder   | string   | 渲染的inputplaceholder值                                     | 否                                                      |
| initValue     | string   | 默认初始value值                                              | 否                                                      |
| data          | array    | 下拉列表数据的初始化本地值                                   | 否                                                      |
| url           | string   | 下拉列表数据异步加载地址                                     | 否                                                      |
| delay         | int      | 搜索延迟毫秒                                                 | 否 (默认200)                                            |
| localSearch   | boolean  | 是否开启本地搜索                                             | 否 (默认true)                                           |
| filterMethod  | function | 本地搜索过滤方法                                             | 否                                                      |
| parseData     | function | 此参数仅在异步加载data数据下或者远程搜索模式下有效，解析回调<br> 此回调函数一定要return数据，否则无法渲染 | 否                                                      |
| remoteSearch  | boolean  | 是否启用远程搜索 默认是false，和远程搜索回调保存同步         | 否 (默认false)                                          |
| remoteMethod  | function | 远程搜索的回调函数                                           | 否                                                      |
| paging        | boolean  | 是否开启分页模式                                             | 否 (默认false)                                          |
| pageSize      | int      | 分页每页显示条数                                             | 否 (默认10)                                             |
| pageRemote    | boolean  | 是否开启远程分页                                             | 否 (默认false)                                          |
| inputClickShow    | boolean  | 是否点击input框自动展开下拉框                                 | 否 (默认true)                                           |
| clickClose    | boolean  | 是否点击选项后自动关闭下拉框                                 | 否 (默认true)                                           |
| prop          | object   | 数据模型                                                     | 否(默认：{ name: 'name', value: 'value' })              |
| showProp   | string   | 点击option之后input里面value值默认属性 | 否(默认：name)                                                  |
| remoteEvent   | string   | 远程搜索响应的事件 （默认为input实时输入事件，目前还支持键盘回车事件，传：keydown） | 否                                                      |
| error         | function | 异步加载出错的回调 回调参数是错误msg                         | 否                                                      |
| done          | function | 异步加载成功后的回调 回调参数加载返回数据                    | 否                                                      |
| show          | function | 下拉框展开回调                                               | 否                                                      |
| hide          | function | 下拉框收起回调                                               | 否                                                      |
| onInput       | function | 实时输入事件                                                 | 否                                                      |
| onClick       | function | 下拉选项点击选择事件                                         | 否                                                      |

#### 组件实例方法

```html
    
   var ins2 = selectInput.render({
        elem: '#test1',
        data: [
            {value: 1111, name: 1111},
            {value: 2333, name: 2222},
            {value: 2333, name: 2333},
            {value: 2333, name: 2333},
        ],
        placeholder: '请输入名称',
        name: 'list_common',
        remoteSearch: false
    });
   
     // 获取选中的value值
    var selectValue = ins.getValue(); // bug已修复
    
    // 清空输入框的value值
    ins.emptyValue();

    // 设置value值
    ins.setValue(1); // bug已经修复

    // 强制重新渲染组件，适用于清空dom之后再更新组件
    ins.refresh()

    // 获取输入框的文本内容
    ins.getText()

    // 获取选择之后的内容，为对象形式 {name："",value:"",isSelect: true}
    ins.getSelect()

   // 动态添加select选项，第二个默认为false，即为覆盖data重载，true为push追加
   // 远程分页和远程搜索不支持追加数据
    ins.append([
        {value: 1111, name: 1111},
        {value: 2333, name: 2222},
        {value: 2333, name: 2333},
        {value: 2333, name: 2333},
        {value: 6666, name: 9999},
        {value: 8888, name: 101010},
    ],false);
    
    // 展开下拉框
    ins.opened();

   // 关闭下拉框
   ins.closed();

```