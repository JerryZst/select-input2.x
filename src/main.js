import { default as SelectInput, dataS } from './index.js';

const moduleName = 'selectInput';

if ((typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === 'object') {
    module.exports = SelectInput;
} else if (typeof define === 'function' && define.amd) {
    define(SelectInput);
} else if (window.layui && layui.define) {
    layui.define(function(exports) {
        exports(moduleName, SelectInput);
    });
}

window[moduleName] = SelectInput;