import {h, Component, render} from 'preact'
import {selector, warn, isArray, deepMerge} from '../../utils/index'
import defaultOptions from '../../config/index'
import {dataS, optionData, componentData} from '@/index.js';
import Framework from "../framework";

class SelectInput {

    constructor(options) {
        this.init(options);
    }

    init(options) {
        this.options = defaultOptions();
        this.renderData(options)
    }

    /**
     * 渲染组件
     * @param options
     */
    renderData(options = {}) {
        let _hasData = !!options.data;
        //记录最新的配置项
        this.options = deepMerge(this.options, options);
        this.options.render_success = false;
        //如果dom不存在, 则不进行渲染事项
        let {dom} = this.options;
        if (!dom) {
            warn(`没有找到渲染对象: ${options.elem}, 请检查`)
            return;
        }
        //判断data的数据类型
        let optionsData = this.options.data || [];
        if (typeof (optionsData) === 'function') {
            optionsData = optionsData();
            this.options.data = optionsData;
        }
        if (!isArray(optionsData)) {
            warn(`data数据必须为数组类型, 不能是${typeof (optionsData)}类型`)
            return;
        }
        render(<Framework {...this.options} _update={Date.now()} hasData={_hasData}
                          onReset={this.onReset.bind(this)}/>, dom);
        this.options.render_success = true;
        return this;
    }

    refresh() {
        let ref = componentData[this.options.elem];
        ref.forceUpdate();
    }

    /**
     * 预留方法
     */
    onReset(options = {}) {
        const {elem} = this.options
        options = deepMerge(optionData[elem], options);
        //重新渲染
        this.init(options);
    }

    /**
     * 获取值
     * @returns {{isSelect: boolean, value: *}|*|string}
     */
    getValue() {
        let ref = componentData[this.options.elem];
        return ref.getValue();
    }

    // 获取输入框内容
    getText() {
        let ref = componentData[this.options.elem];
        return ref._value;
    }

    /**
     * 获取select的形式，包含name
     * @returns {{name: string, isSelect: boolean, value: (*|boolean|string|Effect|((error: any) => void))}}
     */
    getSelect() {
        let ref = componentData[this.options.elem];
        let _data = ref.state.data || [];
        let __value = ref.state._value;
        if (ref.inputSearchValue) {
            __value = ref._value;
        }
        let _value = {name: '', value: __value, isSelect: false}
        for (let _i = 0; _i < _data.length; _i++) {
            if (__value === _data[_i].name) {
                _value.name = _data[_i].name;
                _value.value = _data[_i].value;
                _value.isSelect = true;
                break;
            }
            if (__value === _data[_i].value) {
                _value.name = _data[_i].name;
                _value.isSelect = true;
                break;
            }
        }
        return _value;
    }

    /**
     * 设置value
     */
    setValue(value) {
        let ref = componentData[this.options.elem];
        let _data = ref.state.data || [];
        let _value = value;
        let _isValue = false;
        for (let _i = 0; _i < _data.length; _i++) {
            if (value === _data[_i].value) {
                _value = _data[_i].name;
                _isValue = true;
                break;
            }
        }
        ref.inputSearchValue = !_isValue;
        if (!ref.inputSearchValue) {
            ref._value = _value;
        }
        ref.setState({
            value: _value,
            _value: _value,
            showName: _value,
            itemClick: false
        })
        return this;
    }

    /**
     * 清空数据
     * @returns {SelectInput}
     */
    emptyValue() {
        let ref = componentData[this.options.elem];
        ref.setState({
            value: '',
            _value: '',
            showName: '',
            itemClick: false
        }, () => {
            if (ref.props.pageRemote || ref.props.remoteSearch) {
                const _page = 1;
                ref._changePage(_page);
                ref.remoteData(_page, true);
            }
        })
        return this;
    }

    /**
     * 追加数据
     */
    append(_data, is_append = true) {
        if (!isArray(_data)) {
            warn('请传入数组结构...')
            return;
        }
        let ref = componentData[this.options.elem];
        if (ref.props.pageRemote || ref.props.remoteSearch) {
            warn('远程分页和远程搜索不支持追加数据')
            return;
        }
        let __data = ref.state.data
        if (is_append) {
            _data = __data.concat(_data)
        }
        componentData[this.options.elem].setState({
            data: _data
        });
        return this;
    }

    /**
     * 打开方法
     */
    opened() {
        let ref = componentData[this.options.elem];
        !ref.state.show && ref._iconShow();
        return this;
    }

    /**
     * 关闭方法
     */
    closed() {
        let ref = componentData[this.options.elem];
        ref.state.show && ref._iconShow();
        return this;
    }
}

export default SelectInput;
