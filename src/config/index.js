export default function () {
    return {
        elem: '',
        url: '',
        method: 'get',
        params: {},
        statusCode: 'code',
        statusOK: 0,
        statusData: 'data',
        statusMessage: 'msg',
        //多选数据
        data: [],
        initValue: '',
        //表单提交的name
        name: 'select',
        height: '200px',
        layFilter: '',
        //表单验证
        layVerify: '',
        //验证类型
        layVerType: '',
        //验证提示
        layReqText: '',
        //是否点击隐藏模式
        hasSelectIcon: false,
        //实例方法getValue返回数据模式
        invisibleMode: false,
        // 组件渲染完是否立即展开下拉选项
        hasInitShow: false,
        // 是否启用input剪贴事件
        hasCut: true,
        // 前端联想匹配是否忽略大小写
        ignoreCase: false,
        // 渲染的input placeholder值
        placeholder: '请输入',
        // 实例id
        uniqueId: '',
        //搜索延迟 ms
        delay: 200,
        remoteEvent: '',
        //本地搜索过滤方法
        filterMethod: function (val, item, index, prop, ignoreCase) {
            if (!val) return true;
            let _name = item[prop.name];
            let _value = val;
            // 如果是忽略大小写比较，统一转为小写
            if (ignoreCase) {
                _name = _name.toLowerCase();
                _value = _value.toLowerCase();
            }
            return _name.indexOf(_value) != -1;
        },
        // 默认开启本地搜索
        localSearch: true,
        //是否开启远程搜索
        remoteSearch: false,
        //远程搜索回调
        remoteMethod: function (val, cb) {
            cb([]);
        },
        //是否开启分页
        paging: false,
        //分页每页的条数
        pageSize: 10,
        //是否开启远程分页
        pageRemote: false,
        //是否点击选项后自动关闭下拉框
        clickClose: true,
        inputClickShow: true, // 是否点击input自动下拉框
        showProp: 'name',
        isPureSelectMode: false,
        //自定义属性名称
        prop: {
            name: 'name',
            value: 'value'
        },
        // 展开下拉框
        show() {

        },
        // 隐藏下拉框
        hide() {

        },
        // 解析回调
        parseData(data) {
            return data
        },
        // 监听选中事件
        on({arr, item, selected}) {

        },
        // 实时输入事件
        onInput(value) {

        },
        /**
         * 离焦事件
         */
        onBlur(item) {

        },
        /**
         * 聚焦事件
         */
        onFocus(item) {

        },
        /**
         * option 点击事件
         */
        onClick(item) {

        },
        // 渲染完毕事件
        done() {

        },
        // 远程加载失败事件
        error() {

        }
    }
}
